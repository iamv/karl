# Introduction


# Usage

## initialize

```shell
python main.py run
```

# API


# URI 

## home page

```
${DOMAIN}/
```

## containers info

```
${DOMAIN}/img
${DOMAIN}/images

${DOMAIN}/img/lst?c=<num>&p=<>&d=f
${DOMAIN}/images/list?count=<num>&pagination=<>&details=full

${DOMAIN}/img/insp?<image_tag>
${DOMAIN}/image/inspect?<image_tag>

${DOMAIN}/img/hist?<image_tag>
${DOMAIN}/image/history?<image_tag>
```

## networks 

```
${DOMAIN}/netw/
${DOMAIN}/networks/

${DOMAIN}/netw/ls
${DOMAIN}/networks/list
```

```
${DOMAIN}/vol/
${DOMAIN}/volumes/

${DOMAIN}/vol/ls
${DOMAIN}/volumes/list
```
